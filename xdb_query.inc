<?php

class xdb_query extends views_plugin_query_default {
  protected $primary_database;
  
  // An array storing the fields that needs to be selected by the auxilliary
  // queries. First key is the database, second is the field alias as set by
  // Views and the value is the field array, again as set by Views.
  protected $xdb_select_fields = array();
  
  // This is the id we would JOIN on if the tables werent in two different
  // databases. Same structure as $xdb_select_fields just the values are simple
  // the name of the ID field.
  protected $xdb_id_field = array();
  
  protected $id_tables = array();

  function add_orderby($table, $field, $order, $alias = '') {
    $this->id_tables[] = $table;
    return parent::add_orderby($table, $field, $order, $alias);
  }
  
  function build(&$view) {
    $aliases = array();
    foreach ($this->table_queue as $table_alias => &$table_data) {
      $table_name = $table_data['table'];
      $table_definition = views_fetch_data($table_name);
      $database = $table_definition['table']['database'];
      $table_data['database'] = $database;
      $aliases[] = $table_alias;
    }
    $preg = '/(?!<\w)(' . implode('|', $aliases) . ')\./';
    foreach ($this->where as $group) {
      foreach ($group['clauses'] as $key => $clause) {
        preg_match_all($preg, $clause, $matches);
        $this->id_tables = array_merge($this->id_tables, $matches[1]);
      }
    }
    $id_tables_flipped = array_flip($this->id_tables);
    $original_base_field = $this->base_field;
    $original_base_table = $this->base_table;
    foreach ($this->fields as $field_alias => $field) {
      if ($field['table'] && !isset($id_tables_flipped[$field['table']])) {
        $database = $this->table_queue[$field['table']]['database'];
        $this->xdb_select_fields[$database][$field_alias] = $field;
        unset($this->fields[$field_alias]);
        if ($field_alias == $this->base_field) {
          unset($this->base_table);
        }
      }
    }
    if (!isset($this->base_table)) {
      // Now let's add a new base table.
      $field = reset($this->fields);
      $table = $field['table'];
      $this->base_table = $table;
      $data = $this->table_queue[$table];
      // Let's try to figure out which field are we joining on.
      if (is_object($data['join'])) {
        $this->base_field = $data['join']->field;
      }
      // We need to add this explicitly because the id query otherwise would not
      // contain the id.
      $this->add_field($this->base_table, $this->base_field);
      // As the base_table appears in FROM, we do not want it JOINed.
      unset($this->table_queue[$this->base_table]);
    }
    else {
      $data = $this->table_queue[$this->base_table];
    }
    if ($data['database'] != 'default') {
      $view->base_database = $data['database'];
    }
    foreach ($this->table_queue as $table_alias => $table_data) {
      if (!isset($id_tables_flipped[$table_alias])) {
        $id_field = $table_alias == $original_base_table ? $original_base_field : $table_data['join']->field; 
        $this->xdb_id_field[$table_data['database']][$table_alias] = $id_field;
        unset($this->table_queue[$table_alias]);
      }
    }
    parent::build($view);
  }

  function execute(&$view) {
    // Run the id query.
    parent::execute($view);
    $ids = array();
    $base_field = $this->base_field;
    $placeholder = '%d';
    foreach ($view->result as $key => $result) {
      $id = $result->$base_field;
      if (!is_numeric($id)) {
        $placeholder = "'%s'";
      }
      $ids[] = $id;
      $id_map[$id] = $key;
    }
    $placeholders = implode(', ', array_fill(0, count($ids), $placeholder));
    foreach ($this->xdb_select_fields as $database => $fields_array) {
      // Every field gets its own query. Not perfect but a start.
      foreach ($fields_array as $field) {
        $string = $this->compile_field($field);
        $id_field = $this->xdb_id_field[$database][$field['table']];
        db_set_active($database);
        $result = db_query("SELECT $string, $id_field FROM {" . $field['table'] . "} WHERE $id_field IN ($placeholders)", $ids);
        $key = $field['alias'];
        while ($data = db_fetch_object($result)) {
          $view->result[$id_map[$data->$id_field]]->$key = $data->$key;
        }
        db_set_active();
      }
    }
  }
  
  /**
   * This function compiles a field array into the string to be added to SELECT.
   *
   * It's unlikely it needs to be overridden.
   * @TODO: remove when http://drupal.org/node/619812 is committed.
   */
  function compile_field($field) {
    $this->has_aggregate = FALSE;
    $this->non_aggregates = array();
    $string = '';
    if (!empty($field['table'])) {
      $string .= $field['table'] . '.';
    }
    $string .= $field['field'];
    // store for use with non-aggregates below
    $fieldname = (!empty($field['alias']) ? $field['alias'] : $string);

    if (!empty($field['distinct'])) {
      $string = "DISTINCT($string)";
    }
    if (!empty($field['count'])) {
      $string = "COUNT($string)";
      $this->has_aggregate = TRUE;
    }
    elseif ($this->distinct && !in_array($fieldname, $this->groupby)) {
      $string = $GLOBALS['db_type'] == 'pgsql' ? "FIRST($string)" : $string;
    }
    else {
      $this->non_aggregates[] = $fieldname;
    }
    if ($field['alias']) {
      $string .= " AS $field[alias]";
    }
    return $string;
  }
  
}
